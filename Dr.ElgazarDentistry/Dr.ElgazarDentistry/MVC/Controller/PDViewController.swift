//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
let patientStoryBoard : UIStoryboard = UIStoryboard(name: "Patient", bundle: NSBundle.mainBundle())
let patientArabiStoryBoard : UIStoryboard = UIStoryboard(name: "PatientArabi", bundle: NSBundle.mainBundle())


class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        buttonBack?.hidden = isFromPreviousForm && buttonSubmit == nil
        buttonSubmit?.backgroundColor = UIColor.greenColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "DR. ELGAZAR DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            if self.patient.selectedForms.first!.formTitle! == "NOTICE OF PRIVACY PRACTICES"{
                                self.patient.selectedForms.removeFirst()
                            }
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    
    func gotoNextForm(showBackButton : Bool) {
        if isFromPreviousForm {
            if patient.selectedForms.count > 0 { patient.selectedForms.removeFirst() }
        }
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(knewPatientEnglish){
            let new1VC = patientStoryBoard.instantiateViewControllerWithIdentifier("PatientEng1VC") as! PatientEng1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(knewPatientArabic){
                let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi1Vc") as! PatientInformationArabi1Vc
                new1VC.patient = self.patient
                self.navigationController?.pushViewController(new1VC, animated: true)

        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kEndondonticConsent){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("EndodonticEnglishVC") as! EndodonticConsentEnglishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kEndondonticConsentSpanish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("EndodonticSpanishVC") as! EndodonticConsentSpanishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOralEnglish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("OralEnglishVC") as! OralEnglishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOralSpanish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("OralSpanishVC") as! OralSpanishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kPerioEnglish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("PerioEnglishVC") as! PerioEnglishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kPerioSpanish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("PerioSpanishVC") as! PerioSpanishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kRestorativeEnglish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("RestorativeEnglishVC") as! RestorativeEnglishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kRestorativeSpanish){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("RestorativeSpanishVC") as! RestorativeSpanishViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kTeethWhitening){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("Teeth1VC") as! TeethWhitening1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kEndondonticTreatment){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("EndodonticTreatmentVc") as! EndodonticTreatmentVc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
       }
    }
}
