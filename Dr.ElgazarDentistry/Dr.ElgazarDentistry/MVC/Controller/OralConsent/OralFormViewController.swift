//
//  OralFormViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralFormViewController: PDViewController {

    @IBOutlet var imageviewSignature : UIImageView!
    @IBOutlet var labelDate : UILabel!
    var patientSign : UIImage!
    var formImage : UIImage!
    @IBOutlet var imageViewForm : UIImageView!

    @IBOutlet weak var PatientName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        imageviewSignature.image = patientSign
        imageViewForm.image = formImage
        
        PatientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
