//
//  PatientEng7ViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientEng7ViewController: PDViewController {
    
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet var radioButtonSelf: RadioButton!
    @IBOutlet var textFieldEmployerName: PDTextField!
    @IBOutlet var textFieldOccupation: PDTextField!
    @IBOutlet var radioButtonInsurance: RadioButton!
    @IBOutlet var insuranceName: PDTextField!
    @IBOutlet var insurancePhone: PDTextField!
    @IBOutlet var insuranceId: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioButtonInsurance(sender: UIButton) {
        if sender.tag == 2{
            viewContainer.userInteractionEnabled = false
            viewContainer.alpha = 0.5
             insuranceName.text = ""
            insurancePhone.text = ""
            insuranceId.text = ""
        }else{
            viewContainer.userInteractionEnabled = true
            viewContainer.alpha = 1.0
            
        }
    }
    
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if radioButtonInsurance.selectedButton == nil{
            let alert = Extention.alert("PLEASE SELECT YOUR INSURANCE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioButtonInsurance.selected && insuranceName.isEmpty && insurancePhone.isEmpty && insuranceId.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
   
        } else if !insurancePhone.isEmpty && !insurancePhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
        let new1VC = patientStoryBoard.instantiateViewControllerWithIdentifier("PatientEng8ViewController") as! PatientEng8ViewController
        patient.employerSelfButton = radioButtonSelf.selectedButton == nil ? 0 : radioButtonSelf.selectedButton.tag
        patient.employerName = textFieldEmployerName.isEmpty ? "N/A" : textFieldEmployerName.text
        patient.employerOccupation = textFieldOccupation.isEmpty ? "N/A" : textFieldOccupation.text
        patient.insuranceButton = radioButtonInsurance.selectedButton == nil ? 0 : radioButtonInsurance.selectedButton.tag
        patient.insuranceName = insuranceName.isEmpty ? "N/A" : insuranceName.text
        patient.insurancePhone = insurancePhone.isEmpty ? "N/A" : insurancePhone.text
        patient.insuranceid = insuranceId.isEmpty ? "N/A" : insuranceId.text
        new1VC.patient = self.patient
        self.navigationController?.pushViewController(new1VC, animated: true)
        }
}
}
extension PatientEng7ViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == insurancePhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
