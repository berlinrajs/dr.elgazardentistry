//
//  PatientEng6ViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientEng6ViewController: PDViewController {
    
   
    @IBOutlet var textFieldStreet: PDTextField!
    @IBOutlet var textFieldApartment: PDTextField!
    @IBOutlet var textFieldCity: PDTextField!
    @IBOutlet var textFieldState: PDTextField!
    @IBOutlet var textFieldZipCode: PDTextField!
    @IBOutlet var textFieldTime: PDTextField!
    @IBOutlet var radioButtonGender: RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    DateAndTimeInputView.addDatePickerForTextField(textFieldTime)
        StateListView.addStateListForTextField(textFieldState)
        
        if patient.responsiblePartyButton == 1 {
        
        textFieldStreet.text = patient.patientInformation.streetAddress
        textFieldApartment.text = patient.patientInformation.apartmentNumber
        textFieldCity.text = patient.patientInformation.city
        textFieldState.text = patient.patientInformation.state
        textFieldZipCode.text = patient.patientInformation.zipcode
        radioButtonGender.setSelectedWithTag(patient.patientInformation.genderTag!)
        
        
        }else {
        
            textFieldStreet.text = ""
            textFieldApartment.text = ""
            textFieldCity.text = ""
            textFieldState.text = ""
            textFieldZipCode.text = ""
            radioButtonGender.setSelectedWithTag(0)

        
        
        }
        

        // Do any additional setup after loading the view.
    }
    @IBAction func radioButtonAction(sender: AnyObject) {
        sender.setSelectedWithTag(sender.tag)
        
        
        
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        
        
       if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else{

            let new1VC = patientStoryBoard.instantiateViewControllerWithIdentifier("PatientEng7ViewController") as! PatientEng7ViewController
       
        patient.responsiblePartyStreet = textFieldStreet.isEmpty ? "N/A" : textFieldStreet.text
        patient.responsiblePartyApartment = textFieldApartment.isEmpty ? "N/A" :textFieldApartment.text
        patient.responsiblePartyStreet = textFieldStreet.isEmpty ? "N/A" : textFieldStreet.text
        patient.responsiblePartyCity = textFieldCity.isEmpty ? "N/A" : textFieldCity.text
        patient.responsiblePartyState = textFieldState.isEmpty ? "N/A" : textFieldState.text
        patient.responsiblePartyZip = textFieldZipCode.isEmpty ? "N/A" : textFieldZipCode.text
        patient.responsiblePartyTimeToCall = textFieldTime.isEmpty ? "N/A" : textFieldTime.text
        patient.responsiblePartyGenderButton = radioButtonGender.selectedButton == nil ? 0 : radioButtonGender.selectedButton.tag
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PatientEng6ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

    
    
    


