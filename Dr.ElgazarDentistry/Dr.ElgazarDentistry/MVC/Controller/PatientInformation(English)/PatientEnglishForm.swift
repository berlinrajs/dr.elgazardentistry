//
//  PatientEnglishForm.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientEnglishForm: PDViewController {
    //PatientInformation
    @IBOutlet var labelPatientLastName: UILabel!
    @IBOutlet var labelPatientFirstName: UILabel!
    @IBOutlet var labelPatientMiddleName: UILabel!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelGender: UILabel!
    @IBOutlet var radioButtonMaritalStatus: RadioButton!
    @IBOutlet var labelSSC: UILabel!
    @IBOutlet var labelBirthDate: UILabel!
    @IBOutlet var labelHome: UILabel!
    @IBOutlet var labelCell: UILabel!
    @IBOutlet var labelWork: UILabel!
    @IBOutlet var labelExt: UILabel!
    @IBOutlet var labelStreet: UILabel!
    @IBOutlet var labelApartment: UILabel!
    @IBOutlet var labelCity: UILabel!
    @IBOutlet var labelState: UILabel!
    @IBOutlet var labelZip: UILabel!
    @IBOutlet var labelEmail: UILabel!
    //HealthInformation
    
    @IBOutlet var labelDentalVisit: UILabel!
    @IBOutlet var labelReasonForVisit: UILabel!
    @IBOutlet var arrayofButton1: [RadioButton]!
     @IBOutlet var arrayofButton2: [RadioButton]!
     @IBOutlet var arrayofButton3: [RadioButton]!
    @IBOutlet var labelOtherAllergies: [UILabel]!
    @IBOutlet var radioButtonDentalTreatment: RadioButton!
    @IBOutlet var labelDentalTreatment: UILabel!
    @IBOutlet var labelPregencyDueDate: UILabel!
    @IBOutlet var labelRespiratoryProblems: UILabel!
    @IBOutlet var labelOther: UILabel!
    @IBOutlet var radioButtonEmergency: RadioButton!
    @IBOutlet var labelDentalEmergency: UILabel!
    @IBOutlet var radioButtonPhysicianCare: RadioButton!
    @IBOutlet var labelPhysicianCare: UILabel!
    @IBOutlet var labelNameOfphysician: UILabel!
    
    @IBOutlet var labelPhysicianPhone: UILabel!
    @IBOutlet var radioButtonHealthClarification:
    RadioButton!
    @IBOutlet var labelHealthClarification: UILabel!
    @IBOutlet var labelMedications: UILabel!
    @IBOutlet var labelEmergencyContact: UILabel!
    @IBOutlet var labelEmergencyNo: UILabel!
    @IBOutlet var labelHearAboutOffice: UILabel!
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet var labelDate2: UILabel!
    
    //ResponsibleParty
    @IBOutlet var radioButtonResponsibleSelf: RadioButton!
    @IBOutlet var labelResName: UILabel!
    @IBOutlet var labelResSSC: UILabel!
    @IBOutlet var labelResBirthDate: UILabel!
    @IBOutlet var radioButtonResGender: RadioButton!
    @IBOutlet var labelResHomePhone: UILabel!
    @IBOutlet var labelResWorkPhone: UILabel!
    @IBOutlet var labelResExt: UILabel!
    @IBOutlet var labelResTimeToCall: UILabel!
    @IBOutlet var labelResStreet: UILabel!
    @IBOutlet var labelResApartment: UILabel!
    @IBOutlet var labelResCity: UILabel!
    @IBOutlet var labelResState: UILabel!
    @IBOutlet var labelResZip: UILabel!
    
    //EmploymentInformation
    @IBOutlet var radioButtonEmploySelf: RadioButton!
    @IBOutlet var labelEmpName: UILabel!
    @IBOutlet var labelEmpOccupation: UILabel!
    
    //InsuranceInformation
    @IBOutlet var labelInsName: UILabel!
    @IBOutlet var labelInsPhone: UILabel!
    @IBOutlet var labelInsId: UILabel!
    
    //OfficePolicy
    @IBOutlet var imageViewSignature2: UIImageView!
    @IBOutlet var imageViewSignature3: UIImageView!
    @IBOutlet var imageViewSignature4: UIImageView!
    @IBOutlet var labelDate3: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //PatientInformation
        labelPatientFirstName.text = patient.firstName
        labelPatientLastName.text = patient.lastName
        labelPatientMiddleName.text = patient.middleInitial
        labelDate.text = patient.dateToday
        labelGender.text = patient.patientInformation.gender
        radioButtonMaritalStatus.setSelectedWithTag(patient.buttonMarital!)
        labelSSC.text = patient.patientInformation.socialSecurityNumber
        labelBirthDate.text = patient.dateOfBirth
        labelHome.text = patient.patientInformation.homeNumber
        labelCell.text = patient.patientInformation.cellNumber
        labelWork.text = patient.patientInformation.workNumber
        labelExt.text = patient.patientInformation.extensionNumber
        labelStreet.text = patient.patientInformation.streetAddress
        labelApartment.text = patient.patientInformation.apartmentNumber
        labelCity.text = patient.patientInformation.city
        labelState.text = patient.patientInformation.state
        labelZip.text = patient.patientInformation.zipcode
        labelEmail.text = patient.patientInformation.email
        
        //HealthInformation
        labelDentalVisit.text = patient.healthLastDentalVisit
        labelReasonForVisit.text = patient.textViewCommments

        for btn in arrayofButton1{
            let ques : PDQuestion = patient.patientInformation.medicalQuestion[0][btn.tag]
            btn.selected = ques.selectedOption
        }
        for btn in arrayofButton2{
            let ques : PDQuestion = patient.patientInformation.medicalQuestion[1][btn.tag]
            btn.selected = ques.selectedOption
        }
        for btn in arrayofButton3{
            let ques : PDQuestion = patient.patientInformation.medicalQuestion[2][btn.tag]
            btn.selected = ques.selectedOption
        }
        patient.patientInformation.medicalQuestion[0][11].answer.setTextForArrayOfLabels(labelOtherAllergies)
        
        labelPregencyDueDate.text = patient.patientInformation.medicalQuestion[1][15].answer
        labelOther.text =  patient.patientInformation.medicalQuestion[2][11].answer


        
        
        radioButtonDentalTreatment.setSelectedWithTag(patient.buttontag1!)
        labelDentalTreatment.text = patient.popupText1
        radioButtonEmergency.setSelectedWithTag(patient.buttontag2!)
        labelDentalEmergency.text = patient.popupText2
        radioButtonPhysicianCare.setSelectedWithTag(patient.buttontag3!)
        labelPhysicianCare.text = patient.popupText3
        labelNameOfphysician.text = patient.healthNameOfPhysician
        labelPhysicianPhone.text = patient.healthPhysicianPhone
        radioButtonHealthClarification.setSelectedWithTag(patient.buttontag4!)
        labelHealthClarification.text = patient.popupText4
        labelMedications.text = patient.healthMedicationTaken
        labelEmergencyContact.text = patient.healthemergencyName
        labelEmergencyNo.text = patient.healthemergencyNo
        labelHearAboutOffice.text = patient.healthAboutOffice
        imageViewSignature.image = patient.healthSignature
        labelDate2.text = patient.dateToday
        
        //ResponsibleParty
        radioButtonResponsibleSelf.setSelectedWithTag(patient.responsiblePartyButton)
        labelResName.text = patient.responsiblePartyName
        labelResSSC.text = patient.responsiblePartySSC
        labelResBirthDate.text = patient.responsiblePartyBirthDate
       radioButtonResGender.setSelectedWithTag(patient.responsiblePartyGenderButton)
        labelResHomePhone.text = patient.responsiblePartyHome
        labelResWorkPhone.text = patient.responsiblePartyWork
        labelResExt.text = patient.responsiblePartyExt
        labelResTimeToCall.text = patient.responsiblePartyTimeToCall
        labelResStreet.text = patient.responsiblePartyStreet
        labelResApartment.text = patient.responsiblePartyApartment
        labelResCity.text = patient.responsiblePartyCity
        labelResState.text = patient.responsiblePartyState
        labelResZip.text = patient.responsiblePartyZip
        
        //EmploymentInformation
        radioButtonEmploySelf.setSelectedWithTag(patient.employerSelfButton)
        labelEmpName.text = patient.employerName
        labelEmpOccupation.text = patient.employerOccupation
        
        
        // Insurance Information
        labelInsName.text = patient.insuranceName
        labelInsPhone.text = patient.insurancePhone
        labelInsId.text = patient.insuranceid
        
        //OfficePolicy
        imageViewSignature2.image = patient.officialPolicySig1
        imageViewSignature3.image = patient.officialPolicySig2
        imageViewSignature4.image = patient.officialPolicySig3
        labelDate3.text = patient.dateToday
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
