//
//  PatientEng8ViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 19/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientEng8ViewController: PDViewController {
    
    @IBOutlet var imageViewSignature1: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var imageViewSignature3: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    labelDate.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !imageViewSignature1.isSigned() || !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = patientStoryBoard.instantiateViewControllerWithIdentifier("PatientEnglishForm") as! PatientEnglishForm
            
            patient.officialPolicySig1 = imageViewSignature1.image
            patient.officialPolicySig2 = imageViewSignature2.image
            patient.officialPolicySig3 = imageViewSignature3.image
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
}
}