//
//  RestorativeFormViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RestorativeFormViewController: PDViewController {
    @IBOutlet var imageviewSignature : UIImageView!
    @IBOutlet var labelDate : UILabel!
    var patientSign : UIImage!
    var formImage : UIImage!
    @IBOutlet var imageViewForm : UIImageView!

    @IBOutlet weak var PatietnName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        imageviewSignature.image = patientSign
        imageViewForm.image = formImage
        
        PatietnName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
