//
//  PatientInformation5Vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi5Vc: PDViewController {

    @IBOutlet var textFieldResponsiblePartyName: PDTextField!
    @IBOutlet var textFieldSSN: PDTextField!
    @IBOutlet var textFieldBirthDate: PDTextField!
    @IBOutlet var textFieldHome: PDTextField!
    @IBOutlet var radioButton: RadioButton!
    
    @IBOutlet var textFieldExt: PDTextField!
    
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DateInputView.addDatePickerForTextField(textFieldBirthDate)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func radioButtonAction(sender: AnyObject) {
        sender.setSelectedWithTag(sender.tag)
        
        if sender.tag == 1 {
            
            textFieldResponsiblePartyName.text = patient.fullName
            textFieldSSN.text = patient.patientInformation.socialSecurityNumber
            textFieldBirthDate.text = patient.dateOfBirth
            textFieldHome.text = patient.patientInformation.homeNumber
            textFieldExt.text = patient.patientInformation.extensionNumber
            textFieldWorkPhone.text = patient.patientInformation.workNumber
            
        } else {
            
            
            textFieldResponsiblePartyName.text = ""
            textFieldSSN.text = ""
            textFieldBirthDate.text = ""
            textFieldHome.text = ""
            textFieldExt.text = ""
            textFieldWorkPhone.text = ""
            
            
            
        }
        
    }
    
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !textFieldHome.isEmpty && !textFieldHome.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID HOME NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textFieldSSN.isEmpty && textFieldSSN.text!.characters.count != 9{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if  !textFieldExt.isEmpty && textFieldExt.text!.characters.count != 3 {
            let alert = Extention.alert("PLEASE ENTER THE VALID EXTENSION NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID WORK NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi6Vc") as! PatientInformationArabi6Vc
            patient.responsiblePartyExt = textFieldExt.isEmpty ? "N/A" : textFieldExt.text
            patient.responsiblePartyName = textFieldResponsiblePartyName.isEmpty ? "N/A" : textFieldResponsiblePartyName.text
            patient.responsiblePartySSC = textFieldSSN.isEmpty ? "N/A" : textFieldSSN.text
            patient.responsiblePartyBirthDate = textFieldBirthDate.isEmpty ? "N/A" : textFieldBirthDate.text
            patient.responsiblePartyHome = textFieldHome.isEmpty ? "N/A" :textFieldHome.text
            patient.responsiblePartyWork = textFieldWorkPhone.isEmpty ? "N/A" : textFieldWorkPhone.text
            patient.responsiblePartyButton = radioButton.selectedButton == nil ? 0 : radioButton.selectedButton.tag
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PatientInformationArabi5Vc : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldHome || textField == textFieldWorkPhone
        {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textFieldSSN {
            return textField.formatNumbers(range, string: string, count: 9)
        }else if textField == textFieldExt{
            return textField.formatNumbers(range, string: string, count: 3)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


