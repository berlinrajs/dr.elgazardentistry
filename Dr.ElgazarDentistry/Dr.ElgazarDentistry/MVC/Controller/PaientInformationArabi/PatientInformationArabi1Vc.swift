//
//  PatientInformationArabi1Vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi1Vc: PDViewController {

    @IBOutlet weak var textfieldStreet : UITextField!
    @IBOutlet weak var textfieldApartment : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldExt : UITextField!
    @IBOutlet weak var textfieldHomeNumber : UITextField!
    @IBOutlet weak var textfieldCellNumber : UITextField!
    @IBOutlet weak var textfieldWorkNumber : UITextField!
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var genderButton : RadioButton!
    var arrayGender = ["" , "Male", "Female" , "Other"]
    
    @IBOutlet var radioButtonMarital: RadioButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StateListView.addStateListForTextField(textfieldState)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if textfieldStreet.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || genderButton.selectedButton == nil || radioButtonMarital.selectedButton == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !textfieldExt.isEmpty && textfieldExt.text!.characters.count != 3{
            let alert = Extention.alert("PLEASE ENTER THE VALID EXTENSION NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !textfieldHomeNumber.isEmpty && !textfieldHomeNumber.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID HOME NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if (!textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber){
            let alert = Extention.alert("PLEASE ENTER THE VALID CELL NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if (!textfieldWorkNumber.isEmpty && !textfieldWorkNumber.text!.isPhoneNumber){
            let alert = Extention.alert("PLEASE ENTER THE VALID WORK NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !textfieldSSN.isEmpty && textfieldSSN.text!.characters.count != 9{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            let alert = Extention.alert("PLEASE ENTER THE VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)
            
        }else{
            patient.patientInformation.streetAddress = textfieldStreet.text!
            patient.patientInformation.apartmentNumber = textfieldApartment.isEmpty ? "N/A" : textfieldApartment.text!
            patient.patientInformation.city = textfieldCity.text!
            patient.patientInformation.state = textfieldState.text!
            patient.patientInformation.zipcode = textfieldZipcode.text!
            patient.patientInformation.extensionNumber = textfieldExt.isEmpty ? "" : textfieldExt.text!
            patient.patientInformation.homeNumber = textfieldHomeNumber.text!
            patient.patientInformation.cellNumber = textfieldCellNumber.isEmpty ? "" : textfieldCellNumber.text!
            patient.patientInformation.workNumber = textfieldWorkNumber.isEmpty ? "" : textfieldWorkNumber.text!
            patient.patientInformation.socialSecurityNumber = textfieldSSN.isEmpty ? "" : textfieldSSN.text!
            patient.patientInformation.email = textfieldEmail.isEmpty ? "" : textfieldEmail.text!
            patient.patientInformation.genderTag = genderButton.selectedButton.tag
            patient.patientInformation.gender = arrayGender[genderButton.selectedButton.tag]
            patient.buttonMarital = radioButtonMarital.selectedButton.tag
            let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi2vc") as! PatientInformationArabi2vc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    
    
}

extension PatientInformationArabi1Vc : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldZipcode {
            return textField.formatZipCode(range, string: string)
        }else if textField == textfieldExt{
            return textField.formatNumbers(range, string: string, count: 3)
        }else if textField == textfieldHomeNumber || textField == textfieldCellNumber || textField == textfieldWorkNumber
        {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldSSN{
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
