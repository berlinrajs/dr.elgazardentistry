//
//  PatientInformationArabi2vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi2vc: PDViewController {

    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var buttonVerified: UIButton!
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.stopAnimating()
    }
    
    @IBAction func onBackButtonPressed (sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            buttonBack1.userInteractionEnabled = false
            buttonNext.userInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1.userInteractionEnabled = true
                self.buttonNext.userInteractionEnabled = true
            }
            
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if buttonVerified.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 2 {
                let formVC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi3Vc") as! PatientInformationArabi3Vc
                formVC.patient = self.patient
                self.navigationController?.pushViewController(formVC, animated: true)
            } else {
                buttonBack1.userInteractionEnabled = false
                buttonNext.userInteractionEnabled = false
                self.buttonVerified.selected = false
                self.activityIndicator.startAnimating()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.userInteractionEnabled = true
                    self.buttonNext.userInteractionEnabled = true
                }
            }
        }
    }
    
}

extension PatientInformationArabi2vc : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.patientInformation.medicalQuestion2[selectedIndex].count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.patientInformation.medicalQuestion2[selectedIndex][indexPath.row])
        return cell
    }
    
}

extension PatientInformationArabi2vc : PatientInfoCellDelegate{
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        if selectedIndex == 0 && cell.tag == 5 || selectedIndex == 2 && cell.tag == 11{
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE EXPLAIN", completion: { (textView, isEdited) in
                if isEdited{
                    cell.question.answer = textView.text!
                    
                }else{
                    cell.question.answer = "N/A"
                    cell.question.selectedOption = false
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            
        }else if selectedIndex == 1 && cell.tag == 15{
            PopupTextField.sharedInstance.showDatePopupWithTitle("PLEASE ENTER YOUR DUE DATE", placeHolder: "DUE DATE", minDate: nil, maxDate: nil, completion: { (textField, isEdited) in
                if isEdited{
                    cell.question.answer = textField.text!
                }else{
                    cell.question.answer = "N/A"
                    cell.question.selectedOption = false
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
                
            })
            
        }
    }
    
}
