//
//  PatientInformationArabi8Vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi8Vc: PDViewController {

    
    @IBOutlet var imageViewSignature1: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var imageViewSignature3: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var imageViewSignature4: SignatureView!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        
        labelDate1.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !imageViewSignature1.isSigned() || !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned() || !imageViewSignature4.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped || !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabiForm") as! PatientInformationArabiForm
            patient.officialPolicySig1 = imageViewSignature1.image
            patient.officialPolicySig2 = imageViewSignature2.image
            patient.officialPolicySig3 = imageViewSignature3.image
             patient.officialPolicySig4 = imageViewSignature4.image
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
}