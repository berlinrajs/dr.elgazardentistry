//
//  PatientInformationArabi3Vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi3Vc: PDViewController {

    @IBOutlet var textFieldLastDentalVisit: PDTextField!
    @IBOutlet var textViewComments: PDTextView!
    @IBOutlet var textFieldPhysicianName: PDTextField!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet var textFieldPhone: PDTextField!
    @IBOutlet var radiobutton1: RadioButton!
    @IBOutlet var radiobutton2: RadioButton!
    @IBOutlet var radiobutton3: RadioButton!
    @IBOutlet var radiobutton4: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DateInputView.addDatePickerForTextField(textFieldLastDentalVisit)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
        
        
    }
    
    @IBAction func radioButtonAction(sender: RadioButton) {
        sender.setSelectedWithTag(sender.tag)
        
        if sender.tag == 1
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("If yes, Please explain") { (textView, isEdited) in
                if isEdited {
                    self.patient.popupText1 = textView.text
                    
                }
                else {
                    sender.selected = false
                }
            }
            
        }else if sender.tag == 2
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("If yes, Please explain") { (textView, isEdited) in
                if isEdited {
                    self.patient.popupText2 = textView.text
                } else {
                    sender.selected = false
                }
            }
        } else if sender.tag == 3
        {
            PopupTextView.sharedInstance.showWithPlaceHolder("If yes, Please explain") { (textView, isEdited) in
                if isEdited {
                    self.patient.popupText3 = textView.text
                } else {
                    sender.selected = false
                }
            }
            
        } else if sender.tag == 4 {
            PopupTextView.sharedInstance.showWithPlaceHolder("If yes, Please explain") { (textView, isEdited) in
                if isEdited {
                    self.patient.popupText4 = textView.text
                } else {
                    sender.selected = false
                }
            }
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !buttonVerified.selected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else{
            let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi4Vc") as! PatientInformationArabi4Vc
            patient.buttontag1 = radiobutton1.selectedButton.tag
            patient.buttontag2 = radiobutton2.selectedButton.tag
            patient.buttontag3 = radiobutton3.selectedButton.tag
            patient.buttontag4 = radiobutton4.selectedButton.tag
            patient.healthLastDentalVisit = textFieldLastDentalVisit.isEmpty ? "N/A" : textFieldLastDentalVisit.text
            patient.textViewCommments = textViewComments.text == "TYPE HERE" ? " " : textViewComments.text
            
            patient.healthNameOfPhysician = textFieldPhysicianName.isEmpty ? "N/A" : textFieldPhysicianName.text
            patient.healthPhysicianPhone = textFieldPhone.isEmpty ? "N/A" : textFieldPhone.text
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
}

extension PatientInformationArabi3Vc: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInformationArabi3Vc : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "If yes, Please explain" || textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            //                textView.text = textViewComments.text
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

