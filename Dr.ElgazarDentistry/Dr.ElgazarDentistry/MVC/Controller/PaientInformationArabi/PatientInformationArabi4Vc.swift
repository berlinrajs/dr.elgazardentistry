//
//  PatientInformationArabi4Vc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 01/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientInformationArabi4Vc: PDViewController {

    @IBOutlet var textViewComments1: PDTextView!
    @IBOutlet var textViewComments2: PDTextView!
    @IBOutlet var textFieldEmergencyName: PDTextField!
    @IBOutlet var textFieldEmergencyContactNo: PDTextField!
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if textFieldEmergencyName.isEmpty || textFieldEmergencyContactNo.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if !textFieldEmergencyContactNo.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !imageViewSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = patientArabiStoryBoard.instantiateViewControllerWithIdentifier("PatientInformationArabi5Vc") as! PatientInformationArabi5Vc
            
            patient.healthMedicationTaken = textViewComments1.text == "TYPE HERE" ? "" : textViewComments1.text
//            patient.healthAboutOffice = textViewComments2.text == "TYPE HERE" ? "N/A" : textViewComments2.text
            patient.healthemergencyName = textFieldEmergencyName.isEmpty ? "" : textFieldEmergencyName.text
            patient.healthemergencyNo = textFieldEmergencyContactNo.isEmpty ? "N/A" : textFieldEmergencyContactNo.text
            patient.healthSignature = imageViewSignature.image
            
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
    }
    
}

extension PatientInformationArabi4Vc : UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEmergencyContactNo {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInformationArabi4Vc : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "If yes, Please explain" || textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            //                textView.text = textViewComments.text
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

