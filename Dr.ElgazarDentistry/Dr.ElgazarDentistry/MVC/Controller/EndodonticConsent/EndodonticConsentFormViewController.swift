//
//  EndodonticConsentFormViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticConsentFormViewController: PDViewController {

    @IBOutlet var imageviewSignature : UIImageView!
    @IBOutlet var labelDate : UILabel!
    var patientSign : UIImage!
    var formImage : UIImage!
    @IBOutlet var imageViewForm : UIImageView!
    
    @IBOutlet weak var PatientName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        imageviewSignature.image = patientSign
        imageViewForm.image = formImage
        
        PatientName.text = patient.fullName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
