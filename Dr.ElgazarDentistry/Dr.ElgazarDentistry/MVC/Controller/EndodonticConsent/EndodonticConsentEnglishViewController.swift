//
//  EndodonticConsentEnglishViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticConsentEnglishViewController: PDViewController {

    @IBOutlet weak var imageViewSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if !imageViewSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("EndodonticFormVC") as! EndodonticConsentFormViewController
            new1VC.patient = self.patient
            new1VC.formImage = UIImage(named: "EndodonticEnglish")
            new1VC.patientSign = imageViewSignature.signatureImage()
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
        
    }
}
