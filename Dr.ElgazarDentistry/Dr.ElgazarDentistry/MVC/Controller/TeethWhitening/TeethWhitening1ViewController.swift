//
//  TeethWhitening1ViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TeethWhitening1ViewController: PDViewController {
    @IBOutlet weak var imageViewSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        let string : NSString = labelPatientName.text!.stringByReplacingOccurrencesOfString("KPATIENTNAME", withString: patient.fullName)
        let range = string.rangeOfString(patient.fullName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        labelPatientName.attributedText = attributedString

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !imageViewSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("TeethFormVC") as! TeethWhiteningFormViewController
            new1VC.patient = self.patient
            new1VC.patientSign = imageViewSignature.signatureImage()
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
        
    }


}
