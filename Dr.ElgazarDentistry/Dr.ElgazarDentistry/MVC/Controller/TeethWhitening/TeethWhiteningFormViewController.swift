//
//  TeethWhiteningFormViewController.swift
//  Dr.ElgazarDentistry
//
//  Created by Bala Murugan on 8/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TeethWhiteningFormViewController: PDViewController {

    @IBOutlet var imageviewSignature : UIImageView!
    @IBOutlet var labelDate : UILabel!
    var patientSign : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!

    @IBOutlet weak var PatientName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        imageviewSignature.image = patientSign
        labelPatientName.text = patient.fullName
        
        PatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
