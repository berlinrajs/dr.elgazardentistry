//
//  EndodonticTreatmentForm.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticTreatmentForm: PDViewController {
    @IBOutlet weak var imageViewInitials1 : UIImageView!
    @IBOutlet weak var imageViewInitials2 : UIImageView!
    @IBOutlet weak var imageViewInitials3 : UIImageView!
    @IBOutlet weak var imageViewInitials4 : UIImageView!
    @IBOutlet weak var imageViewInitials5 : UIImageView!
    @IBOutlet weak var imageViewInitials6 : UIImageView!
    @IBOutlet weak var imageViewInitials7 : UIImageView!
    @IBOutlet weak var imageViewInitials8 : UIImageView!
    @IBOutlet weak var imageViewInitials9 : UIImageView!
    @IBOutlet weak var imageViewInitials10 : UIImageView!
    @IBOutlet weak var imageViewInitials11 : UIImageView!
    @IBOutlet weak var imageViewInitials12 : UIImageView!
    @IBOutlet weak var imageViewPatientSignature : UIImageView!
    @IBOutlet weak var imageViewDoctorsSignature : UIImageView!
    @IBOutlet weak var imageViewWitnessSignature : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelDoctorName1 : UILabel!
    @IBOutlet weak var labelDoctorName2 : UILabel!
    @IBOutlet weak var labelDoctorName3 : UILabel!
    @IBOutlet weak var labelDoctorName4 : UILabel!
    
    @IBOutlet weak var PatietnName: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewInitials1.image = patient.initialSignature1
        imageViewInitials2.image = patient.initialSignature2
        imageViewInitials3.image = patient.initialSignature3
        imageViewInitials4.image = patient.initialSignature4
        imageViewInitials5.image = patient.initialSignature5
        imageViewInitials6.image = patient.initialSignature6
        imageViewInitials7.image = patient.initialSignature7
        imageViewInitials8.image = patient.initialSignature8
        imageViewInitials9.image = patient.initialSignature9
        imageViewInitials10.image = patient.initialSignature10
        imageViewInitials11.image = patient.initialSignature11
        imageViewInitials12.image = patient.initialSignature12
        imageViewPatientSignature.image = patient.patientSignature
        imageViewDoctorsSignature.image = patient.doctorsSignature
        imageViewWitnessSignature.image = patient.witnessSignature 
        labelDoctorName1.text = "Ahmed Elgazar"
        labelDoctorName2.text = "Ahmed Elgazar"
        labelDoctorName3.text = "Ahmed Elgazar"
        labelDoctorName4.text = "Ahmed Elgazar"
        labelDate.text = patient.dateToday
        
        PatietnName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
