//
//  EndodonticTreatmentVc.swift
//  Dr.ElgazarDentistry
//
//  Created by SRS Web Solutions on 18/08/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticTreatmentVc: PDViewController {
    @IBOutlet var labelDoctorNameChange: UILabel!
    @IBOutlet var labelDoctorNameChange1: UILabel!
    @IBOutlet var labelDoctorNameChange2: UILabel!
    @IBOutlet var labelDoctorNameChange3: UILabel!
    @IBOutlet weak var imageViewInitials1 : SignatureView!
    @IBOutlet weak var imageViewInitials2 : SignatureView!
    @IBOutlet weak var imageViewInitials3 : SignatureView!
    @IBOutlet weak var imageViewInitials4 : SignatureView!
    @IBOutlet weak var imageViewInitials5 : SignatureView!
    @IBOutlet weak var imageViewInitials6 : SignatureView!
    @IBOutlet weak var imageViewInitials7 : SignatureView!
    @IBOutlet weak var imageViewInitials8 : SignatureView!
    @IBOutlet weak var imageViewInitials9 : SignatureView!
    @IBOutlet weak var imageViewInitials10 : SignatureView!
    @IBOutlet weak var imageViewInitials11 : SignatureView!
    @IBOutlet weak var imageViewInitials12 : SignatureView!
    @IBOutlet weak var imageViewPatientSignature : SignatureView!
    @IBOutlet weak var imageViewDoctorsSignature : SignatureView!
    @IBOutlet weak var imageViewWitnessSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
         labelDoctorNameChange.text = labelDoctorNameChange.text!.stringByReplacingOccurrencesOfString("kDoctorsName", withString: "\(patient.doctorName)")
        let string : NSString = labelDoctorNameChange.text!
        let range = string.rangeOfString(patient.doctorName )
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        labelDoctorNameChange.attributedText = attributedString
        
        labelDoctorNameChange1.text = labelDoctorNameChange1.text!.stringByReplacingOccurrencesOfString("kDoctorsName", withString: "\(patient.doctorName)")
        let string4 : NSString = labelDoctorNameChange1.text!
        let range4 = string4.rangeOfString(patient.doctorName )
        let attributedString4 = NSMutableAttributedString(string: string4 as String)
        attributedString4.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range4)
        labelDoctorNameChange1.attributedText = attributedString4

        
      
        
        labelDoctorNameChange2.text = labelDoctorNameChange2.text!.stringByReplacingOccurrencesOfString("kDoctorsName", withString: "\(patient.doctorName)")
         let string2 : NSString = labelDoctorNameChange2.text!
         let range2 = string2.rangeOfString(patient.doctorName )
        let attributedString2 = NSMutableAttributedString(string: string2 as String)
         attributedString2.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range2)
        labelDoctorNameChange2.attributedText = attributedString2
        
        
        labelDoctorNameChange3.text = labelDoctorNameChange3.text!.stringByReplacingOccurrencesOfString("kDoctorsName", withString: "\(patient.doctorName)")
        let string3 : NSString = labelDoctorNameChange3.text!
        let range3 = string3.rangeOfString(patient.doctorName )
        let attributedString3 = NSMutableAttributedString(string: string3 as String)
        attributedString3.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range3)
         labelDoctorNameChange3.attributedText = attributedString3

      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton) {
        if !imageViewPatientSignature.isSigned() || !imageViewDoctorsSignature.isSigned() || !imageViewWitnessSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("EndodonticTreatmentForm") as! EndodonticTreatmentForm
            patient.initialSignature1 = imageViewInitials1.image
            patient.initialSignature2 = imageViewInitials2.image
            patient.initialSignature3 = imageViewInitials3.image
            patient.initialSignature4 = imageViewInitials4.image
            patient.initialSignature5 = imageViewInitials5.image
            patient.initialSignature6 = imageViewInitials6.image
            patient.initialSignature7 = imageViewInitials7.image
            patient.initialSignature8 = imageViewInitials8.image
            patient.initialSignature9 = imageViewInitials9.image
            patient.initialSignature10 = imageViewInitials10.image
            patient.initialSignature11 = imageViewInitials11.image
            patient.initialSignature12 = imageViewInitials12.image
            patient.patientSignature = imageViewPatientSignature.image
            patient.doctorsSignature = imageViewDoctorsSignature.image
             patient.witnessSignature = imageViewWitnessSignature.image
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }
        
    }
}


