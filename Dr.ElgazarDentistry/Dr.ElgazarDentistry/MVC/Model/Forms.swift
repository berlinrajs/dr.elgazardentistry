//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kConsentForms = "CONSENT FORMS"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let knewPatientEnglish = "NEW PATIENT SIGN IN FORM (ENGLISH)"
let knewPatientArabic = "NEW PATIENT SIGN IN FORM (ARABIC)"

let kEndondonticConsent = "ENDODONTIC CONSENT (ENGLISH)"
let kEndondonticConsentSpanish = "ENDODONTIC CONSENT (SPANISH)"
let kOralEnglish = "ORAL SURGERY CONSENT (ENGLISH)"
let kOralSpanish = "ORAL SURGERY CONSENT (SPANISH)"
let kPerioEnglish = "PERIO CONSENT (ENGLISH)"
let kPerioSpanish = "PERIO CONSENT (SPANISH)"
let kRestorativeEnglish = "RESTORATIVE CONSENT (ENGLISH)"
let kRestorativeSpanish = "RESTORATIVE CONSENT (SPANISH)"
let kTeethWhitening = "TEETH WHITENING CONSENT"
let kEndondonticTreatment = "CONSENT FOR ENDODONTIC TREATMENT"

//let toothNumberRequired = [kMedicare]


class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [knewPatientEnglish,knewPatientArabic,kInsuranceCard,kDrivingLicense, kConsentForms]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
    }

    
    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 4 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = false//toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kEndondonticConsent,kEndondonticConsentSpanish,kOralEnglish,kOralSpanish,kPerioEnglish,kPerioSpanish,kRestorativeEnglish,kRestorativeSpanish,kTeethWhitening,kEndondonticTreatment], isSubForm:  true)
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
