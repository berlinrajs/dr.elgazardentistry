//
//  PDPatient.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var middleInitial : String = ""
    var dateToday : String!
    var doctorName: String!
    var dateOfBirth : String!
    var patientSignature : UIImage!
    var doctorsSignature : UIImage!
    var witnessSignature : UIImage!
    var buttonMarital : Int?


    
    
    //HealthInformation
    var healthLastDentalVisit : String!
     var healthReasonDentalVisit : String!
    var popupText1 : String!
    var popupText2 : String!
    var popupText3 : String!
    var popupText4 : String!
    var buttontag1 : Int?
    var buttontag2 : Int?
    var buttontag3 : Int?
    var buttontag4 : Int?
    var textViewCommments : String!
    var healthNameOfPhysician : String!
    var healthPhysicianPhone : String!
    var healthMedicationTaken : String!
    var healthemergencyName : String!
    var healthemergencyNo : String!
    var healthAboutOffice : String!
    var healthSignature : UIImage!
    //ResponsibleParty
    var responsiblePartyButton : Int!
    var responsiblePartyName : String!
    var responsiblePartySSC : String!
    var responsiblePartyBirthDate : String!
    var responsiblePartyGenderButton : Int!
    var responsiblePartyHome : String!
     var responsiblePartyWork : String!
     var responsiblePartyExt : String!
     var responsiblePartyTimeToCall : String!
     var responsiblePartyCity : String!
     var responsiblePartyState : String!
     var responsiblePartyZip : String!
     var responsiblePartyApartment : String!
     var responsiblePartyStreet : String!
    //Employer Information
     var employerSelfButton : Int!
     var employerName : String!
    var employerOccupation :String!
    //InsuranceInformation
    var insuranceButton : Int!
    var insurancePhone : String!
    var insuranceName : String!
    var insuranceid : String!
    //Official Policy
    var officialPolicySig1 : UIImage!
    var officialPolicySig2 : UIImage!
    var officialPolicySig3 : UIImage!
     var officialPolicySig4 : UIImage!
    
    
    //EndodonticTreatment
    var initialSignature1 : UIImage!
    var initialSignature2 : UIImage!
    var initialSignature3 : UIImage!
    var initialSignature4 : UIImage!
    var initialSignature5 : UIImage!
    var initialSignature6 : UIImage!
    var initialSignature7 : UIImage!
    var initialSignature8 : UIImage!
    var initialSignature9 : UIImage!
    var initialSignature10 : UIImage!
    var initialSignature11 : UIImage!
    var initialSignature12 : UIImage!
    
    var fullName: String {
        return middleInitial == "" ? firstName + " " + lastName : firstName + " " + middleInitial + " " + lastName
    }
    
    //SCAN CARDS
    var frontImage: UIImage?
    var backImage: UIImage?
    

    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var patientInformation : PatientInformation = PatientInformation()
}

class PatientInformation : NSObject {
    var streetAddress : String!
    var apartmentNumber : String!
    var city : String!
    var state : String!
    var zipcode : String!
    var extensionNumber : String!
    var homeNumber : String!
    var cellNumber : String!
    var workNumber : String!
    var socialSecurityNumber : String!
    var email : String!
    var gender : String!
    var genderTag : Int?
    
    var medicalQuestion : [[PDQuestion]]!
    
    var medicalQuestion2 : [[PDQuestion]]!

    
    required override init() {
        super.init()
        let quest1: [String] = ["AIDS",
                                "Anemia",
                                "Arthritis",
                                "Artificial Joints",
                                "Asthma",
                                "Blood Disease",
                                "Cancer",
                                "Latex Allergy",
                                "Codeine Allergy",
                                "Penicillin Allergy",
                                "Fen-Phen Allergy",
                                "Other Allergies",
                                "Diabetes",
                                "Dizziness",
                                "Epilepsy",
                                "Excessive Bleeding"]
        
        let quest2: [String] = ["Fainting",
                                "Glaucoma",
                                "Growths",
                                "Hay Fever",
                                "Head Injuries",
                                "Heart Disease",
                                "Heart Murmur",
                                "Hepatitis",
                                "High Blood Pressure",
                                "Jaundice",
                                "Kidney Disease",
                                "Liver Disease",
                                "Mental Disorders",
                                "Nervous Disorders",
                                "Pacemaker",
                                "Pregnancy"]
        
        let quest3: [String] = ["Radiation Treatment",
                                "Respiratory Problems",
                                "Rheumatic Fever",
                                "Rheumatism",
                                "Sinus Problems",
                                "Stomach Problems",
                                "Stroke",
                                "Tuberculosis",
                                "Tumors",
                                "Ulcers",
                                "Venereal Disease",
                                "Other"]
       
        
        let quest4: [String] = ["ا يدز","الحساسيةال ت كس","الحساس يةال كودي ين","الحساسيةال بنسل ين","الحساسيةكاي سي-ال فين","الحساسيةكاي سيال فين","ا ن يميا","المفاصلال تهاب","ا صطناعيةالمفاصل","الربو","الدمأمراض","ال سرطان","السكريمرض","دوخة","ال صرعداء","الم فرطال نزي ف"]
        
        let quest5: [String] = ["إغماء","الزرق","زوائد","ال قشحمى","الرأسإصابات","القلبمرض","تذمرالقلب","الكبدال تهاب","المرت فعالدم ضغط","اليرقان","ال كلىأمراض","الكبدمرض","العقليةاضطرابات","عص ب يةاضطراب ات","ال قلب ضرب اتت نظ يمجهاز","الحملف ترة"]
        let quest6: [String] = ["ا شعاعيالع ج","ن فسيالجهازمشاكل","الرومات يزميةالحمى","روماتزم","ا ن فيةالجيوبمشاكل","الدماغيةالسكتة","المعدةمشاكل","السلمرض","ت ناسليمرض","ا ورام","المعدةقرحة","أخرىط ب يةمشاكل"]
        
        

        
       self.medicalQuestion2 = [PDQuestion.arrayOfQuestions(quest4),PDQuestion.arrayOfQuestions(quest5),PDQuestion.arrayOfQuestions(quest6)]
        self.medicalQuestion2[0][5].isAnswerRequired = true
        self.medicalQuestion2[1][15].isAnswerRequired = true
        self.medicalQuestion2[2].last!.isAnswerRequired = true
        
        
        self.medicalQuestion = [PDQuestion.arrayOfQuestions(quest1),PDQuestion.arrayOfQuestions(quest2),PDQuestion.arrayOfQuestions(quest3)]
        self.medicalQuestion[2].last!.isAnswerRequired = true
        self.medicalQuestion[1].last!.isAnswerRequired = true
        self.medicalQuestion[0][11].isAnswerRequired = true



    }

}

//class PatientInformationArabic : NSObject {
//    var streetAddress : String!
//    var apartmentNumber : String!
//    var city : String!
//    var state : String!
//    var zipcode : String!
//    var extensionNumber : String!
//    var homeNumber : String!
//    var cellNumber : String!
//    var workNumber : String!
//    var socialSecurityNumber : String!
//    var email : String!
//    var gender : String!
//    var genderTag : Int?
//    
//    
//
//}